package main

import (
	"bitbucket.org/struggle888/aurum/executor"
	"bitbucket.org/struggle888/executar/lib"
	"fmt"
	"os"
)

func run() {
	if len(os.Args) == 1 {
		fmt.Println("Short of executor type param~")
		os.Exit(1)
	}
	var e *executor.Executor
	var err error
	switch os.Args[1] {
	case "ip_extend_scan":
		e, err = executor.NewExecutor("ip_extend_scan", "ips", "ipScanResult", 5000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.IPExtendScan)
	case "domain_to_subdomains":
		e, err = executor.NewExecutor("domain_to_subdomains", "domains", "domainSubdomain", 60000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.DomainToSubdomains)
	case "ip_to_cert":
		e, err = executor.NewExecutor("ip_to_cert", "ip443", "ipCertChain", 10000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.IPToCert)
	case "ip_to_domain":
		e, err = executor.NewExecutor("ip_to_domain", "ips", "ipDomain", 3000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.IPToDomain)
	case "get_whois":
		e, err = executor.NewExecutor("get_whois", "ipOrDomain", "whois", 5000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.GetWhois)
	case "domain_to_html":
		e, err = executor.NewExecutor("domain_to_html", "domains", "domainHtml", 8000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.DomainToHtml)
	case "domain_to_rr":
		e, err = executor.NewExecutor("domain_to_rr", "domains", "domainRR", 8000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.DomainToRR)
	case "store_to_mongodb":
		e, err = executor.NewExecutor("store_to_mongodb", "mongoStore", "NULL", 10000)
		if err != nil {
			fmt.Println("Error in init executor: ", err)
			os.Exit(2)
		}
		e.SetExecuteFunction(lib.StoreToMongoDB)



	}

	e.Start()
}

func main() {
	run()
}
