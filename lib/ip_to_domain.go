package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"fmt"
	"github.com/miekg/dns"
	"github.com/tidwall/gjson"
	"math/rand"
	"strings"
)

type ptrResult struct {
	ip      string
	domains []string
}

func IPToDomain(query []string, config string) ([]string, []error) {
	workerAmount := 1000
	if len(query) < workerAmount {
		workerAmount = len(query)
	}
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))     // 输入管道
	output := make(chan ptrResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)       // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go ptrProducer(query, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = ptrOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go ptrConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func ptrConsumer(input <-chan string, output chan<- ptrResult, finish chan<- bool) {
	/* 自定义操作，如：*/
	for {
		ip, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		c := new(dns.Client)

		var result []string

		temp := strings.Split(ip, ".")
		ptrString := strings.Join([]string{temp[3], temp[2], temp[1], temp[0]}, ".") + ".in-addr.arpa"
		b, err := utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], dns.TypePTR, ptrString, true, 2)
		if err == nil {
			records, err := utils.ParseDNSQueryResult(b)
			if err == nil {
				for _, record := range records {
					if record.Type == utils.TypePTR {
						result = append(result, record.Value)
					}
				}
			}
		}

		output <- ptrResult{
			ip:      ip,
			domains: result,
		}
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func ptrProducer(ips []string, input chan<- string) {
	for _, ip := range ips {
		if strings.Index(ip, ".") != -1{
			input <- ip
		}
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func ptrOutputHandler(output <-chan ptrResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}
		if len(result.domains) > 0 {
			results = append(results, result.ip+" "+strings.Join(result.domains, ","))
		}
	}

	//输出结束后通知
	finish <- true
	return results
}
