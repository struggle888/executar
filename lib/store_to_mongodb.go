package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"context"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"github.com/rotisserie/eris"
	"github.com/tidwall/gjson"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"strconv"
	"strings"
	"time"
)

func StoreToMongoDB(records []string, config string) (_ []string, errors []error) {
	if len(records) < 2 {
		return nil, nil
	}

	// 设置客户端连接配置
	mongodbURI := gjson.Get(config, "mongodbURI").String()
	database := gjson.Get(config, "database").String()
	if database == "" {
		database = "Spring_Out"
		fmt.Println("Undefined database, user default:", database)
	}
	clientOptions := options.Client().ApplyURI(mongodbURI)

	// 连接到MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, []error{eris.Wrap(err, "Error in connecting to mongodb")}
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, []error{eris.Wrap(err, "Error in ping to mongodb")}
	}
	db := client.Database(database)

	var certOperations []mongo.WriteModel
	var ipOperations []mongo.WriteModel
	var domainOperations []mongo.WriteModel

	dataType := records[0]
	records = records[1:]
	switch dataType {
	case "ipCertChain":
		deduplicate := map[string]struct{}{}
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=ipCertChain] string:"+record))
				continue
			}
			crts := strings.Split(items[1], ",")
			if len(crts) == 0 {
				continue
			}
			crtBlock, _ := pem.Decode([]byte(crts[0]))
			if crtBlock == nil {
				continue
			}
			x509Cert, err := x509.ParseCertificate(crtBlock.Bytes)
			if err != nil {
				continue
			}
			hash := utils.GetSha256Hash(x509Cert.Raw)
			// 更新IP表
			ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"cert", hash}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from_type", ""}, {"from", ""}}}}).SetUpsert(true))

			// 插入cert表
			chain := bson.A{}
			for _, crt := range crts[1:] {
				chain = append(chain, crt)
			}
			var v interface{}
			if err := json.Unmarshal([]byte(utils.SimpleParsePemCertToJson(crts[0])), &v); err != nil {
				errors = append(errors, eris.Wrapf(err, "Error in unmarshal parsed cert: %s", utils.SimpleParsePemCertToJson(crts[0])))
				continue
			}
			certOperations = append(certOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"hash", hash}}).SetUpdate(bson.D{{"$set", bson.D{{"cert", crts[0]}, {"chain", chain},{"parsed", v}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from_type", "ip"}, {"from", items[0]}}}}).SetUpsert(true))

			// 由证书产生的IP和域名
			for _, ip := range x509Cert.IPAddresses {
				ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", ip}}).SetUpdate(bson.D{{"$setOnInsert", bson.D{{"update_time", time.Now()}, {"from_type", "cert"}, {"from", hash}}}}).SetUpsert(true))
			}
			for _, domain := range x509Cert.DNSNames {
				if strings.HasPrefix(domain, "*.") {
					domain = domain[2:]
				}
				if _, ok := deduplicate[domain]; !ok {
					deduplicate[domain] = struct{}{}
					domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", domain}}).SetUpdate(bson.D{{"$setOnInsert", bson.D{{"update_time", time.Now()}, {"from_type", "cert"}, {"from", hash}}}}).SetUpsert(true))
				}
			}
		}
	case "ipScanResult":
		type NewIP struct {
			ip     string
			fromIP string
			ports  []int
		}
		newIPs := make(map[string]NewIP)
		for _, record := range records {
			items := strings.SplitN(record, " ", 3)
			if len(items) < 3 {
				errors = append(errors, eris.New("short of items???[type=ipScanResult] string:"+record))
				continue
			}
			port, err := strconv.Atoi(items[2])
			if err != nil {
				errors = append(errors, eris.Wrap(err, "Error in convert port to int"))
				continue
			}
			var ports []int
			if _, ok := newIPs[items[1]]; ok {
				ports = append(newIPs[items[1]].ports, port)
			} else {
				ports = append(ports, port)
			}
			newIPs[items[1]] = NewIP{
				ip:     items[1],
				fromIP: items[0],
				ports:  ports,
			}
		}
		for ip, prop := range newIPs {
			p := bson.A{}
			for _, port := range prop.ports {
				p = append(p, port)
			}
			ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", ip}}).SetUpdate(bson.D{{"$set", bson.D{{"ports", p}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from", prop.fromIP}, {"from_type", "ip"}}}}).SetUpsert(true))
		}
	case "domainSubdomain":
		type subdomains []string
		domainSubs := make(map[string]subdomains)
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=domainSubdomain] string:"+record))
				continue
			}
			domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", items[1]}}).SetUpdate(bson.D{{"$setOnInsert", bson.D{{"update_time", time.Now()}, {"from_type", "domain"}, {"from", items[0]}}}}).SetUpsert(true))
			var subs subdomains
			if _, ok := domainSubs[items[0]]; ok {
				subs = append(domainSubs[items[0]], items[1])
			} else {
				subs = append(subs, items[1])
			}
			domainSubs[items[0]] = subs
		}
		for domain, subs := range domainSubs {
			s := bson.A{}
			for _, d := range subs {
				s = append(s, d)
			}
			domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", domain}}).SetUpdate(bson.D{{"$set", bson.D{{"subdomains", s}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from", ""}, {"from_type", ""}}}}).SetUpsert(true))
		}
	case "ipDomain":
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=ipDomain] string:"+record))
				continue
			}
			domains := strings.Split(items[1], ",")
			if len(domains) == 0 {
				continue
			}
			d := bson.A{}
			for _, domain := range domains {
				domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", items[1]}}).SetUpdate(bson.D{{"$setOnInsert", bson.D{{"update_time", time.Now()}, {"from_type", "ip"}, {"from", items[0]}}}}).SetUpsert(true))
				d = append(d, domain)
			}
			ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"domains", d}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from_type", ""}, {"from", ""}}}}).SetUpsert(true))
		}
	case "whois":
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=whois] string:"+record))
				continue
			}
			index := strings.LastIndex(items[0], ".")
			if index == -1 || index == len(items[0])-1 {
				continue
			}
			var v interface{}
			if err := json.Unmarshal([]byte(items[1]), &v); err != nil {
				errors = append(errors, eris.Wrapf(err, "Error in unmarshal whois: %s", items[1]))
				continue
			}
			_, err := strconv.Atoi(items[0][index+1:])
			if err == nil {
				ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"whois", v}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from_type", ""}, {"from", ""}}}}).SetUpsert(true))
			} else {
				domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"whois", v}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from", ""}, {"from_type", ""}}}}).SetUpsert(true))
			}
		}
	case "domainHtml":
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=domainHtml] string:"+record))
				continue
			}
			domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"html", items[1]}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from", ""}, {"from_type", ""}}}}).SetUpsert(true))
		}
	case "domainRR":
		for _, record := range records {
			items := strings.SplitN(record, " ", 2)
			if len(items) < 2 {
				errors = append(errors, eris.New("short of items???[type=domainRR] string:"+record))
				continue
			}
			for _, ipResult := range gjson.Get(items[1], "A").Array(){
				ipOperations = append(ipOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"ip", ipResult.String()}}).SetUpdate(bson.D{{"$setOnInsert", bson.D{{"update_time", time.Now()}, {"from_type", "domain"}, {"from", items[0]}}}}).SetUpsert(true))
			}
			var rr interface{}
			err := json.Unmarshal([]byte(items[1]), &rr)
			if err != nil {
				errors = append(errors, eris.Wrapf(err, "Error in unmarshal rr: %s", items[1]))
				continue
			}
			domainOperations = append(domainOperations, mongo.NewUpdateOneModel().SetFilter(bson.D{{"domain", items[0]}}).SetUpdate(bson.D{{"$set", bson.D{{"rr", rr}, {"update_time", time.Now()}}}, {"$setOnInsert", bson.D{{"from", ""}, {"from_type", ""}}}}).SetUpsert(true))
		}
	}
	if len(ipOperations) > 0 {
		opts := options.BulkWrite().SetOrdered(false)
		_, err := db.Collection("ip").BulkWrite(context.TODO(), ipOperations, opts)
		if err != nil {
			errors = append(errors, eris.Wrap(err, "Error in bulk write ips"))
		}
	}
	if len(domainOperations) > 0 {
		opts := options.BulkWrite().SetOrdered(false)
		_, err := db.Collection("domain").BulkWrite(context.TODO(), domainOperations, opts)
		if err != nil {
			errors = append(errors, eris.Wrap(err, "Error in bulk write domains"))
		}
	}
	if len(certOperations) > 0 {
		opts := options.BulkWrite().SetOrdered(false)
		_, err := db.Collection("cert").BulkWrite(context.TODO(), certOperations, opts)
		if err != nil {
			errors = append(errors, eris.Wrap(err, "Error in bulk write certs"))
		}
	}
	_ = client.Disconnect(context.TODO())
	return nil, errors
}
