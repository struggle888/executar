package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"encoding/json"
	"fmt"
	"github.com/likexian/whois-go"
	whoisparser "github.com/likexian/whois-parser-go"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"strconv"
	"strings"
)

type WhoisResult struct {
	Domain string
	Whois  string
}

var whoisServer map[string]string

func GetWhois(query []string, config string) ([]string, []error) {
	// 初始化whois
	if len(whoisServer) == 0 {
		b, err := ioutil.ReadFile("data/domain-whois.json")
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(b, &whoisServer)
		if err != nil {
			panic(err)
		}
	}

	workerAmount := 100
	if len(query) < workerAmount {
		workerAmount = len(query)
	}
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))       // 输入管道
	output := make(chan WhoisResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)         // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go whoisProducer(query, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = whoisOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go whoisConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func whoisConsumer(input <-chan string, output chan<- WhoisResult, finish chan<- bool) {
	for {
		domain, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		suffix := strings.LastIndex(domain, ".")
		// 是否是IP
		isIP := false
		_, err := strconv.Atoi(domain[suffix+1:])
		if err == nil {
			isIP = true
		}
		result := ""
		if isIP {
			ipWhois, err := whois.Whois(domain)
			if err == nil {
				b, err := json.Marshal(parseRPSL(ipWhois))
				if err == nil {
					result = string(b)
				}
			}

		} else {
			domainWhois, err := whois.Whois(domain, whoisServer[domain[suffix:]])
			if err == nil {
				r, err := whoisparser.Parse(domainWhois)
				if err == nil {
					b, err := json.Marshal(r)
					if err == nil {
						result = string(b)
					}
				}
			}
		}
		if result != "" {
			output <- WhoisResult{
				Domain: domain,
				Whois:  result,
			}

		}
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func whoisProducer(domains []string, input chan<- string) {
	deduplicate := map[string]struct{}{}
	for _, domain := range domains {
		suffix := strings.LastIndex(domain, ".")
		_, err := strconv.Atoi(domain[suffix+1:])
		// 是IP
		if err == nil {
			input <- domain
			continue
		}
		// 是域名
		domainSlice := strings.Split(domain, ".")
		domainSliceLength := len(domainSlice)
		if domainSliceLength < 2{
			continue
		}
		if domainSlice[domainSliceLength - 1] == "cn" && domainSliceLength > 2{
			if utils.IsStringInArray(domainSlice[domainSliceLength - 2], utils.CNSubdomain){
				realDomain := strings.Join(domainSlice[domainSliceLength-3:], ".")
				if _, ok := deduplicate[realDomain]; !ok {
					deduplicate[realDomain] = struct{}{}
					input <- realDomain
				}
				continue
			}
		}
		realDomain := strings.Join(domainSlice[domainSliceLength-2:], ".")
		if _, ok := deduplicate[realDomain]; !ok {
			deduplicate[realDomain] = struct{}{}
			input <- realDomain
		}

		input <- domain
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func whoisOutputHandler(output <-chan WhoisResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}
		results = append(results, strings.Join([]string{result.Domain, result.Whois}, " "))

	}

	//输出结束后通知
	finish <- true
	return results
}

func parseRPSL(raw string) map[string]string {
	result := map[string]string{}
	for _, line := range strings.Split(raw, "\n") {
		if !strings.HasPrefix(line, "#") && !strings.HasPrefix(line, "%") {
			index := strings.Index(line, ":")
			if index != -1 {
				result[line[:index]] = strings.Trim(line[index+1:], " ")
			}
		}
	}
	return result
}
