package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"fmt"
	"github.com/google/uuid"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

type SubdomainResult struct {
	Domain         string
	OutputFilename string
}

func DomainToSubdomains(domains []string, config string) ([]string, []error) {
	workerAmount := 5
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	cmd := exec.Command("chmod", "+x", "bin/subfinder")
	err := cmd.Run()
	utils.IsCriticalError(err)

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))           // 输入管道
	output := make(chan SubdomainResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)             // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go subdomainProducer(domains, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = subdomainOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go subdomainConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func subdomainConsumer(input <-chan string, output chan<- SubdomainResult, finish chan<- bool) {
	/* 自定义操作，如：*/
	for {
		domain, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		randomUUID := uuid.New().String()
		outputFilename := randomUUID + "-subfinder.list"
		cmd := exec.Command("bin/subfinder", "-nW", "-silent", "-timeout", "5", "-t", "100", "-d", domain, "-o", outputFilename, "-max-time", "1")
		err := cmd.Run()
		if err != nil { // 进程异常
			fmt.Println("Error:", err.Error())
			continue
		}
		output <- SubdomainResult{
			Domain:         domain,
			OutputFilename: outputFilename,
		}
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func subdomainProducer(domains []string, input chan<- string) {
	for _, domain := range domains {
		if strings.HasPrefix(domain, "*.") {
			domain = domain[2:]
		}
		input <- domain
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func subdomainOutputHandler(output <-chan SubdomainResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}
		data, err := ioutil.ReadFile(result.OutputFilename)
		utils.IsCriticalError(err)
		if len(data) != 0 {
			data = data[:len(data)-1]
			for _, subd := range strings.Split(string(data), "\n") {
				results = append(results, strings.Join([]string{result.Domain, subd}, " "))
			}
			_ = os.Remove(result.OutputFilename)
		}

	}

	//输出结束后通知
	finish <- true
	return results
}
