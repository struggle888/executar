package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"bytes"
	"crypto/tls"
	"encoding/pem"
	"fmt"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/rotisserie/eris"
	"github.com/tidwall/gjson"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"strings"
	"time"
)

type certResult struct {
	IP    string
	Chain []string
}

func IPToCert(domains []string, config string) ([]string, []error) {
	workerAmount := 100
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))      // 输入管道
	output := make(chan certResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)        // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go certProducer(domains, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = certOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go certConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func certConsumer(input <-chan string, output chan<- certResult, finish chan<- bool) {
	/* 自定义操作，如：*/
	for {
		ip, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		pemCert, err := GetCertificatesPEMFrom(ip + ":443")
		if err == nil {
			chain, _ := getCertChain(pemCert)
			if len(chain) > 0 {
				output <- certResult{
					IP:    ip,
					Chain: chain,
				}
			}
		}
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func certProducer(ips []string, input chan<- string) {
	for _, ip := range ips {
		input <- ip
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func certOutputHandler(output <-chan certResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}
		results = append(results, strings.Join([]string{result.IP, strings.Join(result.Chain, ",")}, " "))
	}

	//输出结束后通知
	finish <- true
	return results
}

func GetCertificatesPEMFrom(address string) (string, error) {

	var errChannel chan error
	errChannel = make(chan error, 2)
	// timeout
	timeout := 5 * time.Second
	time.AfterFunc(timeout, func() {
		errChannel <- eris.New("Timeout")
	})

	conn, err := fasthttp.Dial(address)
	if err != nil {
		return "", eris.Wrap(err, "Failed to establish the tcp connection")
	}
	tlsConn := tls.Client(conn, &tls.Config{
		InsecureSkipVerify: true,
	})
	go func() {
		errChannel <- tlsConn.Handshake()
	}()

	err = <-errChannel

	if err != nil {
		return "", eris.Wrap(err, "Failed to Handshake with tls connection")
	}
	var b bytes.Buffer
	for _, cert := range tlsConn.ConnectionState().PeerCertificates {
		err := pem.Encode(&b, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert.Raw,
		})
		if err != nil {
			return "", eris.Wrap(err, "Failed to encode certificates from the tls connection")
		}
	}
	_ = tlsConn.Close()
	_ = conn.Close()
	return b.String(), nil
}

func getCertChain(pemCert string) ([]string, error) {
	var certChain []string
	block, _ := pem.Decode([]byte(pemCert))
	if block == nil {
		return nil, eris.New("Unable to parse cert")
	}
	next := block.Bytes
	for len(next) > 0 {
		certChain = append(certChain, string(pem.EncodeToMemory(&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: next,
		})))
		var crt utils.Certificate
		err := crt.InitCertificate(next)
		if err != nil {
			return certChain, eris.Wrap(err, "Error in parse cert")
		}

		if utils.PkixNameEqual(crt.Issuer, crt.Subject) {
			//根证书
			return certChain, nil
		}

		if len(crt.Extensions.IssuingCertificateURL) == 0 {
			return certChain, nil
		}
		next = getParent(crt.Extensions.IssuingCertificateURL)
	}
	return certChain, nil
}

func getParent(urls []string) []byte {
	client := retryablehttp.NewClient()
	client.Logger = nil
	for _, url := range urls {
		resp, err := client.Get(url)
		if err != nil || resp.StatusCode/100 != 2 {
			continue
		}
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			continue
		}
		return b
	}
	return []byte{}
}
