package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"bufio"
	"fmt"
	"github.com/google/uuid"
	"github.com/tidwall/gjson"
	"os"
	"os/exec"
	"strings"
)

func IPExtendScan(ips []string, config string) ([]string, []error) {
	var results []string
	var errors []error

	// default config
	rate := 1000
	ports := []string{"21", "22", "23", "25", "80", "443", "110", "135", "445", "554", "8000", "8080", "3306", "6379"}

	//parse config
	if config != "" {
		r := gjson.Get(config, "rate").Int()
		if r != 0 {
			rate = int(r)
		} else {
			fmt.Println("Unable to parse config 'rate' from config string, use default", rate)
		}

		portsArray := gjson.Get(config, "ports").Array()
		var tempArray []string
		for _, port := range portsArray {
			if port.String() != "" {
				tempArray = append(tempArray, port.String())
			}
		}
		if len(tempArray) != 0 {
			ports = tempArray
		} else {
			fmt.Println("Unable to get config 'ports' from config string, use default:", strings.Join(ports, ","))
		}
	}

	// Main
	randomUUID := uuid.New().String()
	confFilename := randomUUID + "-masscan.conf"
	outputFilename := randomUUID + "-masscan.list"
	f, err := os.Create(confFilename)
	utils.IsCriticalError(err)

	// 拓展IPS
	var scanIP []string
	ipMatch := make(map[string]string, len(ips))
	for _, ip := range ips {
		if !strings.Contains(ip, "/") {
			index := strings.LastIndex(ip, ".")
			if index != -1 {
				scanIP = append(scanIP, ip[:index]+".0/24")
				ipMatch[ip[:index]] = ip
			}
		}
		// 不处理CIDR地址
	}

	_, err = f.WriteString(fmt.Sprintf("rate = %d\nports = %s\noutput-format = list\noutput-status = open\noutput-filename = %s\nrange=%s\n", rate, strings.Join(ports, ","), outputFilename, strings.Join(scanIP, ",")))
	utils.IsCriticalError(err)
	err = f.Sync()
	utils.IsCriticalError(err)
	err = f.Close()
	utils.IsCriticalError(err)
	// Run
	cmd := exec.Command("chmod", "+x", "bin/masscan")
	err = cmd.Run()
	utils.IsCriticalError(err)
	cmd = exec.Command("bin/masscan", "-c", confFilename)
	fmt.Println("Start run port scan...")
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()
	err = cmd.Start()
	utils.IsCriticalError(err)
	go utils.PrintOnStdout(stdoutIn)
	go utils.PrintOnStdout(stderrIn)

	err = cmd.Wait()
	if err != nil { // 扫描进程异常
		return results, []error{err}
	}
	// 读取输出
	f, err = os.Open(outputFilename)
	utils.IsCriticalError(err)
	r := bufio.NewReader(f)
	for {
		line, err := utils.ReadLine(r)
		if err != nil {
			break
		}
		if strings.HasPrefix(line, "#") {
			continue
		}
		items := strings.Split(line, " ")
		results = append(results, strings.Join([]string{ipMatch[items[3][:strings.LastIndex(items[3], ".")]], items[3], items[2]}, " "))
	}
	_ = f.Close()
	_ = os.Remove(outputFilename)
	_ = os.Remove(confFilename)
	return results, errors
}
