package lib

import (
	"encoding/base64"
	"fmt"
	"github.com/tidwall/gjson"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

type htmlResult struct {
	Domain string
	HTML   []byte
}

func DomainToHtml(domains []string, config string) ([]string, []error) {
	workerAmount := 10
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))      // 输入管道
	output := make(chan htmlResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)        // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go htmlProducer(domains, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = htmlOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go htmlConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func htmlConsumer(input <-chan string, output chan<- htmlResult, finish chan<- bool) {

	for {
		domain, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		req := fasthttp.AcquireRequest()
		resp := fasthttp.AcquireResponse()
		req.Header.SetMethod("GET")
		req.SetRequestURI("http://" + domain)
		req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36")
		err := fasthttp.DoTimeout(req, resp, 5*time.Second)
		if err == nil {
			output <- htmlResult{
				Domain: domain,
				HTML:   resp.Body(),
			}
		}
		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func htmlProducer(domains []string, input chan<- string) {
	for _, domain := range domains {
		input <- domain
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func htmlOutputHandler(output <-chan htmlResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}
		results = append(results, strings.Join([]string{result.Domain, base64.StdEncoding.EncodeToString(result.HTML)}, " "))
	}

	//输出结束后通知
	finish <- true
	return results
}
