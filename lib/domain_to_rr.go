package lib

import (
	"bitbucket.org/struggle888/executar/utils"
	"encoding/json"
	"fmt"
	"github.com/tidwall/gjson"
	"golang.org/x/net/dns/dnsmessage"
	"math/rand"
	"strings"
)

type SOAResult dnsmessage.SOAResource

type rrRecord struct {
	A     []string    `bson:"A"`
	AAAA  []string    `bson:"AAAA"`
	MX    []string    `bson:"MX"`
	NS    []string    `bson:"NS"`
	CNAME []string    `bson:"CNAME"`
	SOA   []SOAResult `bson:"SOA"`
}

type rrResult struct {
	Domain string
	Record rrRecord
}

func DomainToRR(query []string, config string) ([]string, []error) {
	workerAmount := 100
	if len(query) < workerAmount {
		workerAmount = len(query)
	}
	if config != "" {
		r := gjson.Get(config, "worker_amount").Int()
		if r != 0 {
			workerAmount = int(r)
		} else {
			fmt.Println("Unable to parse config 'worker_amount' from config string, use default", workerAmount)
		}
	}

	/* 初始化管道 */
	input := make(chan string, 10*(workerAmount))    // 输入管道
	output := make(chan rrResult, 10*(workerAmount)) // 结果输出管道
	finish := make(chan bool, (workerAmount)+1)      // 结束信息管道

	var results []string

	/* 启动 */
	fmt.Println("启动~")
	go rrProducer(query, input) // 启动数据输入
	go func() { //启动数据输出处理协程
		results = rrOutputHandler(output, finish)
	}()
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go rrConsumer(input, output, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	fmt.Println("worker已完成全部任务")
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	fmt.Println("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	fmt.Println("完成！")
	return results, nil
}

// 消费者
func rrConsumer(input <-chan string, output chan<- rrResult, finish chan<- bool) {
	/* 自定义操作，如：*/
	for {
		domain, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		result := rrRecord{
			A:     []string{},
			AAAA:  []string{},
			MX:    []string{},
			NS:    []string{},
			CNAME: []string{},
			SOA:   []SOAResult{},
		}
		c := utils.NewDNSClient()

		// 这里放弃使用循环，因为强行使用会用到reflect，很麻烦
		// A
		b, err := utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], utils.TypeA, domain, true, 2)
		if err == nil {
			records, err := utils.ParseDNSQueryResult(b)
			if err == nil {
				for _, record := range records {
					if record.Type == utils.TypeA && record.Key == domain {
						result.A = append(result.A, record.Value)
					}
				}
			}
		}

		// AAAA
		b, err = utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], utils.TypeAAAA, domain, true, 2)
		if err == nil {
			records, err := utils.ParseDNSQueryResult(b)
			if err == nil {
				for _, record := range records {
					if record.Type == utils.TypeAAAA && record.Key == domain {
						result.AAAA = append(result.AAAA, record.Value)
					}
				}
			}
		}

		// MX
		b, err = utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], utils.TypeMX, domain, true, 2)
		if err == nil {
			records, err := utils.ParseDNSQueryResult(b)
			if err == nil {
				for _, record := range records {
					if record.Type == utils.TypeMX && record.Key == domain {
						result.MX = append(result.MX, record.Value)
					}
				}
			}
		}

		// CNAME
		b, err = utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], utils.TypeCNAME, domain, true, 2)
		if err == nil {
			records, err := utils.ParseDNSQueryResult(b)
			if err == nil {
				for _, record := range records {
					if record.Type == utils.TypeCNAME && record.Key == domain {
						result.CNAME = append(result.CNAME, record.Value)
					}
				}
			}
		}

		// SOA
		b, err = utils.QueryDNSRecord(c, utils.DnsServer[rand.Intn(len(utils.DnsServer))], utils.TypeSOA, domain, true, 2)
		if err == nil {
			records, err := utils.ParseSOARecord(b)
			if err == nil {
				for _, record := range records {
					result.SOA = append(result.SOA, SOAResult(record))
				}
			}
		}

		output <- rrResult{
			Domain: domain,
			Record: result,
		}
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func rrProducer(domains []string, input chan<- string) {
	for _, domain := range domains {
		input <- domain
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func rrOutputHandler(output <-chan rrResult, finish chan<- bool) []string {
	var results []string
	for {
		result, ok := <-output
		if ok == false { // 不断读取直到输出管道关闭
			break
		}

		jsonBytes, err := json.Marshal(result.Record)
		if err == nil {
			results = append(results, strings.Join([]string{result.Domain, string(jsonBytes)}, " "))
		}
	}

	//输出结束后通知
	finish <- true
	return results
}

func (soa *SOAResult) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"nameserver":"%s","mbox":"%s","expire":%d,"ttl":%d,"serial":%d,"refresh":%d,"retry":%d}`, strings.TrimRight(soa.NS.String(), "."), strings.TrimRight(soa.MBox.String(), "."), soa.Expire, soa.MinTTL, soa.Serial, soa.Refresh, soa.Retry)), nil
}
