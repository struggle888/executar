FROM debian:stretch-slim
WORKDIR /root
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libpcap0.8 && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* /var/cache/apt/archives/*
COPY bin/ /root/bin/
COPY executar /root/
ENTRYPOINT ["/root/executar"]
