package utils

import (
	"bufio"
	"io"
	"os"
)

func IsStringInArray(s string, a []string) bool {
	for _, i := range a{
		if i == s{
			return true
		}
	}
	return false
}



// 致命错误。需要Debug的
func IsCriticalError(err error){
	if err != nil {
		panic(err)
	}
}

func PrintOnStdout(r io.Reader) ([]byte, error) {
	var out []byte
	buf := make([]byte, 1024, 1024)
	for {
		n, err := r.Read(buf[:])
		if n > 0 {
			d := buf[:n]
			out = append(out, d...)
			_,_ = os.Stdout.Write(d)
		}
		if err != nil {
			// Read returns io.EOF at the end of file, which is not an error for us
			if err == io.EOF {
				err = nil
			}
			return out, err
		}
	}
}

// 工具函数：读取完整一行
func ReadLine(r *bufio.Reader) (string, error) {
	line, isprefix, err := r.ReadLine()
	for isprefix && err == nil {
		var bs []byte
		bs, isprefix, err = r.ReadLine()
		line = append(line, bs...)
	}
	return string(line), err
}
