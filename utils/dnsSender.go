package utils

import (
	"github.com/miekg/dns"
	"github.com/rotisserie/eris"
	"golang.org/x/net/dns/dnsmessage"
	"net"
	"strings"
)

/*
DNS解析结果
*/
type Record struct {
	Type  uint16 // 记录类型
	Key   string // 查询结果名称
	Value string // 查询结果值
}

/*
初始化一个DNS Client,返回DNS Client指针。
|使用github.com/miekg/dns的DNS Client
|一般单个协程使用一个，可同时使用上万Client
*/
func NewDNSClient() *dns.Client {
	return new(dns.Client)
}

/*
查询DNS记录
参数:
	client: dns客户端
    server: 使用的DNS服务器地址
    dnsType: 查询的dns记录类型
    domain: 要查询的域名
    RD: 递归查询(RecursionDesired)是否开启
	timeoutRetry: 超时自动重试次数
*/
func QueryDNSRecord(client *dns.Client, server string, dnsType uint16, domain string, RD bool, timeoutRetry int) ([]byte, error) {
	msg := new(dns.Msg)
	msg.SetQuestion(dns.Fqdn(domain), dnsType)
	msg.RecursionDesired = RD
	var resp *dns.Msg
	var err error
	var i int
	for i = 0; i <= timeoutRetry; i++ {
		resp, _, err = client.Exchange(msg, server)
		if err != nil {
			if strings.Contains(err.Error(), "i/o timeout") {
				continue
			}
			return nil, eris.Wrapf(err, "Error in query dns[server=%s, target=%s]", server, domain)
		}
		break
	}
	if i > timeoutRetry {
		return nil, eris.Wrapf(err, "Error in query dns[server=%s, target=%s]", server, domain)
	}

	result, err := resp.Pack()
	if err != nil {
		return nil, eris.Wrapf(err, "Error in pack response")
	}
	return result, nil
}

/*
解析DNS查询结果，一般和QueryDNSRecord搭配使用。
只能解析出A AAAA CNAME MX NS PTR记录，其它种类记录将被忽略。
防止意外无限循环，最大返回50条记录
参数:
	result: DNS解析结果(with pack)
*/
func ParseDNSQueryResult(result []byte) ([]Record, error) {
	var p dnsmessage.Parser
	_, err := p.Start(result)
	if err != nil {
		return []Record{}, eris.Wrap(err, "Error in parse result")
	}
	err = p.SkipAllQuestions()
	if err != nil {
		return []Record{}, eris.Wrap(err, "Error in skip questions")
	}

	var results []Record
	count := 0
	for {
		if count > 50 {
			break
		} else {
			count++
		}
		h, err := p.AnswerHeader()
		if err == dnsmessage.ErrSectionDone {
			break
		}
		if err != nil {
			return []Record{}, eris.Wrap(err, "Error in parse answer")
		}

		if h.Class != dnsmessage.ClassINET {
			err = p.SkipAnswer()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in skip answer")
			}
			continue
		}

		switch h.Type {
		case dnsmessage.TypeA:
			r, err := p.AResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse A Record")
			}

			results = append(results, Record{
				Type:  TypeA,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: net.IP(r.A[:]).String(),
			})
		case dnsmessage.TypePTR:
			r, err := p.PTRResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse PTR Record")
			}
			results = append(results, Record{
				Type:  TypePTR,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: strings.TrimRight(r.PTR.String(), "."),
			})
		case dnsmessage.TypeCNAME:
			r, err := p.CNAMEResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse CNAME Record")
			}
			results = append(results, Record{
				Type:  TypeCNAME,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: strings.TrimRight(r.CNAME.String(), "."),
			})
		case dnsmessage.TypeMX:
			r, err := p.MXResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse MX Record")
			}
			results = append(results, Record{
				Type:  TypeMX,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: strings.TrimRight(r.MX.String(), "."),
			})
		case dnsmessage.TypeNS:
			r, err := p.NSResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse NS Record")
			}
			results = append(results, Record{
				Type:  TypeNS,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: strings.TrimRight(r.NS.String(), "."),
			})
		case dnsmessage.TypeAAAA:
			r, err := p.AAAAResource()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse AAAA Record")
			}
			results = append(results, Record{
				Type:  TypeAAAA,
				Key:   strings.TrimRight(h.Name.String(), "."),
				Value: net.IP(r.AAAA[:]).String(),
			})
		default:
			err := p.SkipAnswer()
			if err != nil {
				return []Record{}, eris.Wrap(err, "Error in parse NS Record")
			}
		}
	}
	return results, nil
}

/*
只解析SOA记录
参数:
	result: DNS解析结果(with pack)
*/
func ParseSOARecord(result []byte) ([]dnsmessage.SOAResource, error) {
	var p dnsmessage.Parser
	_, err := p.Start(result)
	if err != nil {
		return []dnsmessage.SOAResource{}, eris.Wrap(err, "Error in parse result")
	}
	err = p.SkipAllQuestions()
	if err != nil {
		return []dnsmessage.SOAResource{}, eris.Wrap(err, "Error in skip questions")
	}

	var results []dnsmessage.SOAResource
	count := 0
	for {
		if count > 50 {
			break
		} else {
			count++
		}
		h, err := p.AnswerHeader()
		if err == dnsmessage.ErrSectionDone {
			break
		}
		if err != nil {
			return []dnsmessage.SOAResource{}, eris.Wrap(err, "Error in parse answer")
		}

		if h.Class != dnsmessage.ClassINET {
			err = p.SkipAnswer()
			if err != nil {
				return []dnsmessage.SOAResource{}, eris.Wrap(err, "Error in skip answer")
			}
			continue
		}

		if h.Type == dnsmessage.TypeSOA {
			r, err := p.SOAResource()
			if err != nil {
				return []dnsmessage.SOAResource{}, eris.Wrap(err, "Error in parse SOA record")
			}
			results = append(results, r)
		}
	}
	return results, nil
}

/*
获取目标DNS服务器版本
参数:
	client: dns客户端
    server: 查询的DNS服务器地址
*/
func GetDNSServerVersion(client *dns.Client, server string) ([]string, error) {
	msg := new(dns.Msg)
	msg.Question = make([]dns.Question, 1)
	msg.Question[0] = dns.Question{
		Name:   dns.Fqdn("version.bind"),
		Qtype:  dns.TypeTXT,
		Qclass: dns.ClassCHAOS,
	}
	resp, _, err := client.Exchange(msg, server)
	if err != nil {
		return []string{}, eris.Wrapf(err, "Error in query dns[server=%s]", server)
	}
	result, err := resp.Pack()
	if err != nil {
		return []string{}, eris.Wrap(err, "Error in pack response")
	}
	var p dnsmessage.Parser
	_, err = p.Start(result)
	if err != nil {
		return []string{}, eris.Wrap(err, "Error in parse result")
	}
	err = p.SkipAllQuestions()
	if err != nil {
		return []string{}, eris.Wrap(err, "Error in skip questions")
	}
	h, err := p.AnswerHeader()
	if err == dnsmessage.ErrSectionDone {
		return []string{}, nil
	}
	if err != nil {
		return []string{}, eris.Wrap(err, "Error in parse answer")
	}
	if h.Type == dnsmessage.TypeTXT {
		r, err := p.TXTResource()
		if err != nil {
			return []string{}, eris.Wrap(err, "Error in parse TXT record")
		}
		return r.TXT, nil
	} else {
		return []string{}, nil
	}
}
