package utils

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"github.com/globalsign/mgo/bson"
	"github.com/tidwall/sjson"
	"math/big"
	"net"
	"net/url"
	"strconv"
	"time"
)

// BasicConstraints struct
// The basic constraints extension identifies whether the subject of the
// certificate is a CA and the maximum depth of valid certification
// paths that include this certificate.
type BasicConstraints struct {
	IsCA           bool //是否为CA(默认为False)
	MaxPathLenZero bool //是否可以签发证书
	MaxPathLen     int  //最大可签发的证书级别
}

//NameConstraints must be used only in a CA certificate,
//indicates a name space within which all subject names in
//subsequent certificates in a certification path MUST be located.
type NameConstraints struct {
	PermittedDNSDomainsCritical bool // if true then the name constraints are marked critical.
	PermittedDNSDomains         []string
	ExcludedDNSDomains          []string
	PermittedIPRanges           []string
	ExcludedIPRanges            []string
	PermittedEmailAddresses     []string
	ExcludedEmailAddresses      []string
	PermittedURIDomains         []string
	ExcludedURIDomains          []string
}

//SubjectAlternativeName (SAN) is an extension to X.509
//that allows various values to be associated with a security certificate
//using a subjectAltName field.
type SubjectAlternativeName struct {
	DNSNames       []string //DNS名称
	EmailAddresses []string //EMAIL地址
	IPAddresses    []string //IP地址
	URIs           []string //URL
}

// Extensions of the certificate
type Extensions struct {
	//原内容
	Extensions                  []pkix.Extension
	ExtraExtensions             []pkix.Extension
	UnhandledCriticalExtensions []asn1.ObjectIdentifier
	UnknownExtKeyUsage          []asn1.ObjectIdentifier
	//提取内容
	KeyUsage               []string                // 密钥用法
	BasicConstraints       BasicConstraints        // 基本约束
	SubjectKeyIdentifier   []byte                  // 使用者密钥标识符
	AuthorityKeyIdentifier []byte                  // 授权密钥标识符
	OCSPServer             []string                // OSCP地址
	IssuingCertificateURL  []string                // Issuer证书地址
	SubjectAlternativeName SubjectAlternativeName  // 使用者可选名称
	CRLDistributionPoints  []string                // CRL Distribution Points
	NameConstraints        NameConstraints         // 该CA证书颁 发的所有证书的使用者的名称空间
	ExtendedKeyUsage       []string                // 增强性密钥用法
	PolicyIdentifiers      []asn1.ObjectIdentifier // 证书策略标识符(OID) Related information:http://www.alvestrand.no/objectid/top.html
}

type FingerPrint struct {
	MD5    string // MD5哈希Raw字段
	SHA1   string // SHA1哈希Raw
	SHA256 string // SHA256哈希Raw
}

//"Not Include" OR "Included"
type Trust struct {
	Apple_IOS         string // Apple_IOS是否信任
	Apple_Macos       string // Apple_Macos是否信任
	Google_Aosp       string // Google_Aosp是否信任
	Microsoft_Windows string // Microsoft_Windows是否信任
	Mozilla_NSS       string // Mozilla_NSS是否信任
}

// Certificate struct definition
type Certificate struct {
	//证书内部
	Raw                     []byte     `bson:"raw"`                     // Complete ASN.1 DER content (certificate, signature algorithm and signature).
	RawTBSCertificate       []byte     `bson:"rawtbscertificate"`       // Certificate part of raw ASN.1 DER content.
	RawSubjectPublicKeyInfo []byte     `bson:"rawsubjectpublickeyinfo"` // DER encoded SubjectPublicKeyInfo
	RawSubject              []byte     `bson:"rawsubject"`              // DER encoded Subject
	RawIssuer               []byte     `bson:"rawissuer"`               // DER encoded Issuer
	Signature               []byte     `bson:"signaturegg"`             // 签名信息
	SignatureAlgorithm      string     `bson:"signaturealgorithm"`      // 签名算法
	PublicKeyAlgorithm      string     `bson:"publickeyalgorithm"`      // 公钥算法
	PublicKey               []byte     `bson:"publickey"`               // *公钥
	Version                 int        `bson:"version"`                 // 版本号
	SerialNumber            string     `bson:"serialnumber"`            // 序列号
	Issuer                  pkix.Name  `bson:"issuer"`                  // 颁发者
	Subject                 pkix.Name  `bson:"subject"`                 // 使用者
	NotBefore               time.Time  `bson:"notbefore"`               // 有效期从
	NotAfter                time.Time  `bson:"notafter"`                // 有效期至
	Extensions              Extensions `bson:"extensions"`              // 证书拓展
	//项目定义
	Id          bson.ObjectId `bson:"_id"`
	Fingerprint FingerPrint   `bson:"fingerprint"` //RAW hash by md5
	Trust       Trust         `bson:"trust"`       // 该证书是否在信任列表中 "Not Include" OR "Included"
	FromCrt     bson.ObjectId `bson:"fromcrt"`     // 父证书对应mongodb中的id
	Hosts       []string      `bson:"hosts"`       // 证书所在服务器IP
	IsRevoked   string        `bson:"isrevoked"`   //证书是否被吊销 "Yes","No","Unknown","Root"
}

//GetMD5Hash change raw([]byte) to its md5(string)
func GetMD5Hash(text []byte) string {
	hasher := md5.New()
	hasher.Write(text)
	return hex.EncodeToString(hasher.Sum(nil))
}

func GetSha1Hash(text []byte) string {
	hasher := sha1.New()
	hasher.Write(text)
	return hex.EncodeToString(hasher.Sum(nil))
}

func GetSha256Hash(text []byte) string {
	hasher := sha256.New()
	hasher.Write(text)
	return hex.EncodeToString(hasher.Sum(nil))
}

func InitTrust() Trust {
	var t Trust
	t.Apple_IOS = "Not Include"
	t.Apple_Macos = "Not Include"
	t.Google_Aosp = "Not Include"
	t.Microsoft_Windows = "Not Include"
	t.Mozilla_NSS = "Not Include"
	return t
}

// ParseIPAddresses change ip list([]net.IP) to ip string list([]string)
func ParseIPAddresses(ips []net.IP) []string {
	var ipstring []string
	for _, ip := range ips {
		ipstring = append(ipstring, ip.String())
	}
	return ipstring

}

// ParseIPRanges change net list([]*net.IPNet) to net string list([]string)
func ParseIPRanges(ipnets []*net.IPNet) (netString []string) {
	for _, n := range ipnets {
		netString = append(netString, (*n).String())
	}
	return netString
}

// ParseURIS change url list([]*url.URL) to url string list([]string)
func ParseURIS(urls []*url.URL) []string {
	var urlstring []string
	for _, aurl := range urls {
		urlstring = append(urlstring, (*aurl).String())
	}
	return urlstring
}

// ParseKeyUsage change Keyusage number(x509.KeyUsage(int)) to mean(string)
func ParseKeyUsage(num x509.KeyUsage) []string {
	/*
		CrlSign:可以使用密钥对证书吊销列表 (CRL) 进行签名。
		DataEncipherment:密钥可以用于数据加密。
		DecipherOnly:密钥只能用于解密。
		DigitalSignature:密钥可以用作数字签名。
		EncipherOnly:密钥只能用于加密。
		KeyAgreement:密钥可以用于确定密钥协议，如使用 Diffie-Hellman 密钥协议算法创建的密钥。
		KeyCertSign:可以使用密钥对证书进行签名。
		KeyEncipherment:密钥可以用于密钥加密。
		None:无密钥使用参数。
		NonRepudiation:密钥可以用于身份验证.
	*/
	var KeyUsageList []string
	index := 0
	KeyUsageFindList := []string{"DigitalSignature", "NonRepudiation", "KeyEncipherment", "DataEncipherment", "KeyAgreement", "KeyCertSign", "CrlSign", "EncipherOnly", "DecipherOnly"}
	for num > 0 {
		if int(num)%2 == 1 {
			KeyUsageList = append(KeyUsageList, KeyUsageFindList[index])
		}
		num /= 2
		index++
	}
	return KeyUsageList
}

// ParseExtendedKeyUsage change oid list of ExtKeyUsage([]x509.ExtKeyUsage) to its mean list([]string)
func ParseExtendedKeyUsage(nums []x509.ExtKeyUsage) []string {
	var ExtendedKeyUsageList []string
	ExtendedKeyUsageFindList := []string{"ExtKeyUsageAny", "ServerAuth", "ClientAuth", "CodeSigning", "EmailProtection", "IPSECEndSystem", "IPSECTunnel", "IPSECUser", "TimeStamping", "OCSPSigning", "MicrosoftServerGatedCrypto", "NetscapeServerGatedCrypto", "MicrosoftCommercialCodeSigning", "MicrosoftKernelCodeSigning"}
	for _, ku := range nums {
		ExtendedKeyUsageList = append(ExtendedKeyUsageList, ExtendedKeyUsageFindList[int(ku)])
	}
	return ExtendedKeyUsageList
}

// ParseSerialNumber change SerialNumber(*big.Int) to string
func ParseSerialNumber(num *big.Int) string {
	return (*num).String()
}

// ParsePublicKeyAlgorithm change OID of PublicKeyAlgorithm(x509.PublicKeyAlgorithm) to its mean(string)
func ParsePublicKeyAlgorithm(num x509.PublicKeyAlgorithm) string {
	PublicKeyAlgorithmList := []string{"UnknownPublicKeyAlgorithm", "RSA", "DSA", "ECDSA"}
	return PublicKeyAlgorithmList[int(num)]
}

// ParseSignatureAlgorithm change OID of SignatureAlgorithm(x509.SignatureAlgorithm) to its mean(string)
func ParseSignatureAlgorithm(num x509.SignatureAlgorithm) string {
	SignatureAlgorithmList := []string{"UnknownSignatureAlgorithm", "MD2WithRSA", "MD5WithRSA", "SHA1WithRSA", "SHA256WithRSA", "SHA384WithRSA", "SHA512WithRSA", "DSAWithSHA1", "DSAWithSHA256", "ECDSAWithSHA1", "ECDSAWithSHA256", "ECDSAWithSHA384", "ECDSAWithSHA512", "SHA256WithRSAPSS", "SHA384WithRSAPSS", "SHA512WithRSAPSS"}
	return SignatureAlgorithmList[int(num)]
}

func ParseFingerPrint(raw []byte) FingerPrint {
	var fp FingerPrint
	fp.MD5 = GetMD5Hash(raw)
	fp.SHA1 = GetSha1Hash(raw)
	fp.SHA256 = GetSha256Hash(raw)
	return fp
}

func GetRevokedSerialNumberList(data []byte) ([]string, error) {
	crl, err := x509.ParseCRL(data)
	if err != nil {
		crl, err = x509.ParseDERCRL(data)
		if err != nil {
			return []string{}, err
		}
	}
	var result []string
	for _, crt := range crl.TBSCertList.RevokedCertificates {
		result = append(result, ParseSerialNumber(crt.SerialNumber))
	}
	return result, nil
}

//Init a Certificate by []byte
func (crt *Certificate) InitCertificate(data []byte) error {
	c, err := x509.ParseCertificate(data)
	if err != nil {
		return err
	}
	//直接赋值
	crt.Raw = c.Raw
	crt.RawIssuer = c.RawIssuer
	crt.RawSubject = c.RawSubject
	crt.RawSubjectPublicKeyInfo = c.RawSubjectPublicKeyInfo
	crt.RawTBSCertificate = c.RawTBSCertificate
	crt.Signature = c.Signature
	crt.Version = c.Version
	crt.Issuer = c.Issuer
	crt.Subject = c.Subject
	crt.Extensions.Extensions = c.Extensions
	crt.Extensions.ExtraExtensions = c.ExtraExtensions
	crt.Extensions.UnhandledCriticalExtensions = c.UnhandledCriticalExtensions
	crt.Extensions.UnknownExtKeyUsage = c.UnknownExtKeyUsage
	crt.Extensions.AuthorityKeyIdentifier = c.AuthorityKeyId
	crt.Extensions.SubjectKeyIdentifier = c.SubjectKeyId
	crt.Extensions.OCSPServer = c.OCSPServer
	crt.Extensions.IssuingCertificateURL = c.IssuingCertificateURL
	crt.Extensions.CRLDistributionPoints = c.CRLDistributionPoints
	crt.Extensions.SubjectAlternativeName.DNSNames = c.DNSNames
	crt.Extensions.SubjectAlternativeName.EmailAddresses = c.EmailAddresses
	crt.Extensions.NameConstraints.PermittedDNSDomainsCritical = c.PermittedDNSDomainsCritical
	crt.Extensions.NameConstraints.PermittedDNSDomains = c.PermittedDNSDomains
	crt.Extensions.NameConstraints.ExcludedDNSDomains = c.ExcludedDNSDomains
	crt.Extensions.NameConstraints.PermittedEmailAddresses = c.PermittedEmailAddresses
	crt.Extensions.NameConstraints.ExcludedEmailAddresses = c.ExcludedEmailAddresses
	crt.Extensions.NameConstraints.PermittedURIDomains = c.PermittedURIDomains
	crt.Extensions.NameConstraints.ExcludedURIDomains = c.ExcludedURIDomains
	crt.Extensions.BasicConstraints.IsCA = c.IsCA
	crt.Extensions.BasicConstraints.MaxPathLen = c.MaxPathLen
	crt.Extensions.BasicConstraints.MaxPathLenZero = c.MaxPathLenZero
	crt.NotAfter = c.NotAfter
	crt.NotBefore = c.NotBefore
	crt.Extensions.PolicyIdentifiers = c.PolicyIdentifiers
	crt.IsRevoked = "Unknown"

	//特殊处理
	crt.Id = bson.NewObjectId()
	crt.Fingerprint = ParseFingerPrint(crt.Raw)
	crt.Extensions.KeyUsage = ParseKeyUsage(c.KeyUsage)
	crt.SerialNumber = ParseSerialNumber(c.SerialNumber)
	crt.PublicKeyAlgorithm = ParsePublicKeyAlgorithm(c.PublicKeyAlgorithm)
	crt.SignatureAlgorithm = ParseSignatureAlgorithm(c.SignatureAlgorithm)
	crt.Extensions.SubjectAlternativeName.IPAddresses = ParseIPAddresses(c.IPAddresses)
	crt.Extensions.NameConstraints.PermittedIPRanges = ParseIPRanges(c.PermittedIPRanges)
	crt.Extensions.NameConstraints.ExcludedIPRanges = ParseIPRanges(c.ExcludedIPRanges)
	crt.Extensions.SubjectAlternativeName.URIs = ParseURIS(c.URIs)
	crt.Extensions.ExtendedKeyUsage = ParseExtendedKeyUsage(c.ExtKeyUsage)
	crt.PublicKey, err = x509.MarshalPKIXPublicKey(c.PublicKey)
	crt.Trust = InitTrust()
	if err != nil {
		return err
	}
	return nil

}
func CompareStringSlice (a []string,b []string) int{
	if len(a)>len(b) {
		return 1
	}
	if len(a)<len(b) {
		return -1
	}
	for index,str :=range a {
		if str>b[index]{
			return 1
		}
		if str<b[index]{
			return -1
		}
	}
	return 0
}

func PkixNameEqual(a pkix.Name,b pkix.Name) bool {
	if CompareStringSlice(a.Country,b.Country)!=0 || CompareStringSlice(a.Organization,b.Organization)!=0 || CompareStringSlice(a.OrganizationalUnit,b.OrganizationalUnit)!=0 || CompareStringSlice(a.Locality,b.Locality)!=0 ||CompareStringSlice(a.Province,b.Province)!=0 ||CompareStringSlice(a.StreetAddress,b.StreetAddress)!=0 || CompareStringSlice(a.PostalCode,b.PostalCode)!=0 ||a.SerialNumber!=b.SerialNumber||a.CommonName!=b.CommonName {
		return false
	}
	return true
}

func setJsonValue(str string, key string, value interface{}) string {
	temp, err := sjson.Set(str, key, value)
	if err != nil {
		return str
	} else {
		return temp
	}
}


func SimpleParsePemCertToJson(pemCert string) string{
	jsonCert := ``
	jsonCert = setJsonValue(jsonCert, "pem", pemCert)
	crtBlock, _ := pem.Decode([]byte(pemCert))
	if crtBlock == nil {
		jsonCert = setJsonValue(jsonCert, "metadata.parse_status", "fail")
		return jsonCert
	}
	x509Cert, err := x509.ParseCertificate(crtBlock.Bytes)
	if err != nil {
		jsonCert = setJsonValue(jsonCert, "metadata.parse_status", "fail")
		return jsonCert
	}
	jsonCert = setJsonValue(jsonCert, "validity.not_valid_before", x509Cert.NotBefore.String())
	jsonCert = setJsonValue(jsonCert, "validity.not_valid_after", x509Cert.NotAfter.String())
	jsonCert = setJsonValue(jsonCert, "validity.length", x509Cert.NotAfter.Sub(x509Cert.NotBefore)/time.Second)
	jsonCert = setJsonValue(jsonCert, "fingerprint.md5", GetMD5Hash(x509Cert.Raw))
	jsonCert = setJsonValue(jsonCert, "fingerprint.sha1", GetSha1Hash(x509Cert.Raw))
	jsonCert = setJsonValue(jsonCert, "fingerprint.sha256", GetSha256Hash(x509Cert.Raw))
	jsonCert = setJsonValue(jsonCert, "version", x509Cert.Version)
	jsonCert = setJsonValue(jsonCert, "serial_number", fmt.Sprintf("%x", x509Cert.SerialNumber))
	jsonCert = setJsonValue(jsonCert, "subject_key_info.public_key_algorithm", x509Cert.PublicKeyAlgorithm.String())
	//jsonCert = setJsonValue(jsonCert, "subject_key_info.public_key", x509Cert.PublicKey)
	jsonCert = setJsonValue(jsonCert, "issuer.common_name", x509Cert.Issuer.CommonName)
	jsonCert = setJsonValue(jsonCert, "issuer_dn", x509Cert.Issuer.ToRDNSequence().String())
	jsonCert = setJsonValue(jsonCert, "subject.common_name", x509Cert.Subject.CommonName)
	jsonCert = setJsonValue(jsonCert, "subject_dn", x509Cert.Subject.ToRDNSequence().String())
	jsonCert = setJsonValue(jsonCert, "signature.algorithm", x509Cert.SignatureAlgorithm.String())
	jsonCert = setJsonValue(jsonCert, "signature.value", base64.StdEncoding.EncodeToString(x509Cert.Signature))
	jsonCert = setJsonValue(jsonCert, "extensions.is_ca", x509Cert.IsCA)
	jsonCert = setJsonValue(jsonCert, "extensions.authority_info_access.ocsp", x509Cert.OCSPServer)
	jsonCert = setJsonValue(jsonCert, "extensions.authority_info_access.issuing_certificate_url", x509Cert.IssuingCertificateURL)
	jsonCert = setJsonValue(jsonCert, "extensions.crl_distribution_points", x509Cert.CRLDistributionPoints)
	jsonCert = setJsonValue(jsonCert, "extensions.subject_alt_name.dns_names", x509Cert.DNSNames)
	jsonCert = setJsonValue(jsonCert, "extensions.subject_alt_name.email_addresses", x509Cert.EmailAddresses)
	for index, ip := range x509Cert.IPAddresses{
		jsonCert = setJsonValue(jsonCert, "extensions.subject_alt_name.ip_addresses." + strconv.Itoa(index), ip.String())
	}
	jsonCert = setJsonValue(jsonCert, "extensions.subject_key_identifier", base64.StdEncoding.EncodeToString(x509Cert.SubjectKeyId))
	jsonCert = setJsonValue(jsonCert, "extensions.authority_key_identifier", base64.StdEncoding.EncodeToString(x509Cert.AuthorityKeyId))
	for index, uri := range x509Cert.URIs{
		jsonCert = setJsonValue(jsonCert, "extensions.subject_alt_name.uris." + strconv.Itoa(index), uri.String())
	}

	return jsonCert
}